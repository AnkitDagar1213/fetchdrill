function getUsers(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

function getTodos(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

getUsers("https://jsonplaceholder.typicode.com/users?id=1")
  .then((data) => {
    console.log(data);
    return getTodos("https://jsonplaceholder.typicode.com/todos?id=1");
  })
  .catch((error) => console.log(error))
  .then((data) => console.log(data))
  .catch((error) => console.log(error));
