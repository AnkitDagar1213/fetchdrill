function getTodos(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

getTodos("https://jsonplaceholder.typicode.com/todos?id=1")
  .then((data) => console.log(data))
  .catch((error) => console.log(error));
