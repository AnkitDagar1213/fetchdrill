function getUsers(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data.status == 200) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

function getTodos(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data.status == 200) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

getUsers("https://jsonplaceholder.typicode.com/users")
  .then((data) => {
    console.log(data);
  })
  .catch((error) => console.log(error));
