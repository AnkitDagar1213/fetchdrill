function getUsers(link) {
  return new Promise((resolve, reject) => {
    let data = fetch(link);
    data.then((data) => {
      if (data) {
        resolve(data.json());
      } else {
        reject("error");
      }
    });
  });
}

getUsers("https://jsonplaceholder.typicode.com/users?id=1")
  .then((data) => console.log(data))
  .catch((error) => console.log(error));
